"""
This module provide a Pythonic wrapper around an LMDB database.
It requires no configuration and doesn't use SQL.
The database consists of multiple tables of key value storage.
Keys and values are encoded using msgpack, but this module takes 
care of that for you and you only with with normal Python values.

All reads and writes must be done within a `Transaction`,
which you can obtain like `with db.transact() as tx:`.
A `Transaction` has multiple key value tables stored at keys
accessible using `tx.table(key)` or `tx.tables().
A `Table` offers a dict-like interface to a stored key value table.
You can iterate over entries like `for key, value in table:`
and read or modify its elements like `table[key] = value`.

This library uses LMDB's ability to store keyed sub-databases for
`Table`s and doesn't store other values in the top-level database.

There are three classes:

1. `Connection`: an open LMDB database connection
2. `Transaction`: an open transaction (read or write)
3. `Table`: a key value table within a `Transaction` object

You must use a write transaction to open a table for the first time.

"""
from collections.abc import MutableMapping

import lmdb
from msgpack import packb, unpackb

# Maximum number of tables that can exist.
MAX_TABLES = 128
# Maximum size of database in bytes (default 1 terabyte).
MAX_SIZE = 1_000_000_000_000

class DatabaseException(Exception):
    """`DatabaseException` represents any exception raised by this library."""
    pass

class Connection():
    """
    `Connection` represents an open connection to an LMDB database.
    
    `_path` returns the file path of the database.
    `_env` returns the `lmdb.Environment` representing the database connection.
    If any names are given for named databases, those databases will be 
    created internally and assigned as attributes on the `Connection` object.
    
    We use msgpack to serialize standard python types into binary values.
    It is more efficient to encode/decode and store than JSON.
    """

    def __init__(self, path):
        # The path of the database file.
        self._path = path
        # The open database connection.
        self._env = lmdb.open(path, max_dbs=MAX_TABLES, map_size=MAX_SIZE)
            
    def transact(self, write=False):
        """
        Open and return a read or write transaction on a `Connection`.
        This should only be used in a `with` clause
        to ensure that it gets closed.
        """
        return Transaction(self, write)

class Transaction():
    """
    Transaction is an object that represents an open transaction on a Connection object.
    You should always use a Transaction in a `with` clause to ensure it closes properly.

    A transaction has multiple key value tables each stored at their own key.
    You store values in tables, not in the transaction directly.
    """
    def __init__(self, db, write=False):
        self._write = write
        # Save a handle to the database's lmdb.Environment. 
        self._env = db._env
        
    def __enter__(self):
        # Open a transaction and keep a reference to the lmdb.Transaction object.
        self._transaction = self._env.begin(write=self._write)
        # Open a lmdb.Cursor on the transaction.
        self._cursor = self._transaction.cursor()
        return self
    
    def __exit__(self, type, value, traceback):
        # Commit the transaction if there is no exception.
        if type == None:
            self._transaction.commit()
        # Abort the transaction if there is an exception.
        else:
            self._transaction.abort()
        # Don't suppress any exceptions.
        return False

    def table(self, key=None):
        """
        Get a handle to a `Table` at the given key.
        This is a shortcut for `Table(self, key)`.
        You need to be in a write transaction to open a table the first time.
        """
        return Table(self, key)

    def tables(self):
        """
        Return a list of all table keys.
        """
        tables = []
        for key in self._cursor.iternext(values=False):
            tables.append(unpackb(key))
        return tables

class Table(MutableMapping):
    """
    `Table` is a dict-like object that represents a table within a transaction.
    You must provide a `Transaction` and the key of the table to create a `Table.`
    
    You can use `Table` just like a dict and get the performance of LMDB.
    Any changes you make to a `Table` object in a transaction will be committed
    if there is no exception thrown, or aborted if there is an exception.
    """
    def __init__(self, transaction, key):
        # Save the lmdb.Transaction, lmdb.Environment, and write permissions.
        self._transaction = transaction._transaction
        self._env = transaction._env
        self._write = transaction._write

        # Open and save the child table database.
        self._table = self._env.open_db(packb(key), txn=self._transaction)
        # Save a cursor to the table database.
        self._cursor = lmdb.Cursor(self._table, self._transaction)
        
    def __setitem__(self, key, value):
        # Encode and insert the value.
        return self._cursor.put(packb(key), packb(value))

    def __getitem__(self, key):
        # Retrieve and decode the value.
        value = self._cursor.get(packb(key))
        if value:
            return unpackb(value)
        else:
            return None
    
    def __delitem__(self, key):
        return self._cursor.delete(key)

    def __iter__(self):
        # Position the cursor at the first element.
        self._cursor.first()
        # Use this object as an iterator.
        return self

    def __next__(self):
        # Decode and return the next key and value.
        key = self._cursor.key()
        value = self._cursor.value()
        # Progress the cursor.
        self._cursor.next()
        # The end of the table is indicated by an empty string key.
        if key == b"":
            raise StopIteration
        else:
            return unpackb(key), unpackb(value)

    def __len__(self):
        return self._transaction.stat(self._table)["entries"]
