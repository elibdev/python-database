# Python Database for Formal Web

This is an implementation of a Formal Database as part of the Formal Web system.
It is built using Python and LMDB.

## Python as the Programming Language

Python is a readable, approachable and practical language.
There are Python libraries for almost anything you'd want to do.
Although other languages are faster or better at things
like concurrency, Python has the broadest community.
Since we are striving for accessibility, Python is a good match.

We should try to design the programming interface so that
basic Python coding skills is all you need to customize your app.

## LMDB as the Database

At the center of almost every web service is a database.
Many databases use SQL (Structured Query Language), 
which is another programming language for people to learn.
Key-value databases have become popular because of their simplicity and speed.
A key-value database stores a mapping of key data to value data.
Most relational databases use key-value stores for each table,
so relational databases are actually built on top of them.
Relational databases are difficult to optimize for particular use cases,
while key-value databases allow more direct control over data access.
Key value databases also can have flexible schemas,
which suits Python's dynamic typing well.

A key-value store corresponds with the programming concept of a dictionary,
so in most programming languages is a very intuitive structure to work with.
For these reasons it makes sense to use a key-value store for the database.
[LMDB](http://www.lmdb.tech/doc/) is a very fast transactional key-value
database with support for nested databases stored at keys.
There's also [a great LMDB Python binding](https://lmdb.readthedocs.io).
We can provide a wrapper that provides a dict-like transaction object.
That way you don't need to learn a new database library.

## Database Actions

We can provide a collection of database actions that can be combined
in the same database to enable people to only store the data they want.
The database actions should provide out-of-the-box support for common
usage patterns like storing users and posts.

Actions are simply functions that operate on a database transaction.
They can only interact with each other by modifying the same data.
They are responsible for validating the data that they store.
One advantage of this design is that we can expose only the data
access patterns that we know are going to efficient and useful.

This model feels simpler than ORM (Object Relational Manager) systems
usually associated with the MVC (Model, View, Controller) architecture.
With MVC you need to define the schema in the model layer
and the access patterns separately in the controller layer.
The model layer is not as useful without the controller layer,
since it's external inteface doesn't fully define usage patterns.

Formal Web's actions replace both models and controllers.
Database actions can be added or removed individually on an existing database,
while models require complex and dangerous migrations when the schema changes. 
Actions are more loosely coupled than models, which feels better suited to
Formal Web since it's easier to change in small pieces without breaking things.

Data should be validated at the action layer, and invalid data should raise
an exception with information about how the data is invalid for the action.

## Serialization Format for Stored Values

There are a few data formats we've considered:

- [JSON (JavaScript Object Notation)](https://json.org)
- [MessagePack](https://msgpack.org/)
- [XML (eXtensible Markup Language)](https://developer.mozilla.org/en-US/docs/XML_introduction)
- [Protocol Buffers](https://developers.google.com/protocol-buffers/)

JSON and XML are text-based serialization formats that are used across HTTP.
HTTP is limited to text-based formats, but databases can store binary formats.
MessagePack is like a binary version of JSON and can store data types commonly
used in dynamic languages like maps and arrays.
Protocol Buffers is kind of like a binary XML.

JSON and MessagePack feel well-suited for dynamically typed languages
because there isn't a separate step of declaring your schema.
XML and Protocol Buffers involve first creating strongly typed schemas,
so they feel better suited for statically typed languages.

Since we are using a dynamic language like Python with a database that
supports binary formats, MessagePack feels like the most natural choice.

